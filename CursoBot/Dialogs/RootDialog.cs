﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace CursoBot.Dialogs
{
  [Serializable]
  public class RootDialog : IDialog<object>
  {
    public Task StartAsync(IDialogContext context)
    {
      context.Wait(MessageReceivedAsync);

      return Task.CompletedTask;
    }

    private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
    {
      var activity = await result as Activity;

      var message = activity.CreateReply();

      //Card do tipo herocard
      if (activity.Text.Equals("herocard", StringComparison.InvariantCultureIgnoreCase))
      {
        var herocard = GetHeroCard();
        message.Attachments.Add(herocard);
      }
      if (activity.Text.Equals("thumb", StringComparison.InvariantCultureIgnoreCase))
      {
        var thumb = GetThumbnailCard();
        message.Attachments.Add(thumb);
      }
      if (activity.Text.Equals("gif", StringComparison.InvariantCultureIgnoreCase))
      {
        var gif = GetAnimationCard();
        message.Attachments.Add(gif);
      }
      if (activity.Text.Equals("video", StringComparison.InvariantCultureIgnoreCase))
      {
        var video = GetVideoCard();
        message.Attachments.Add(video);
      }
      if (activity.Text.Equals("audio", StringComparison.InvariantCultureIgnoreCase))
      {
        var audio = GetAudioCard();
        message.Attachments.Add(audio);
      }
      if (activity.Text.Equals("carousel", StringComparison.InvariantCultureIgnoreCase))
      {
        message.AttachmentLayout = AttachmentLayoutTypes.Carousel;

        var herocard = GetHeroCard();
        message.Attachments.Add(herocard);

        var thumb = GetThumbnailCard();
        message.Attachments.Add(thumb);

        var gif = GetAnimationCard();
        message.Attachments.Add(gif);

      }
      if (activity.Text.Equals("receipt", StringComparison.InvariantCultureIgnoreCase))
      {
        var receipt = GetReceiptCard();
        message.Attachments.Add(receipt);
      }
      if (activity.Text.Equals("signin", StringComparison.InvariantCultureIgnoreCase))
      {
        var signin = GetSigninCard();
        message.Attachments.Add(signin);
      }

      //Imprimir
      await context.PostAsync(message);

      //Repetir ele mesmo
      context.Wait(MessageReceivedAsync);
    }

    //Apresentação de imagem grande
    private Attachment GetHeroCard()
    {
      var heroCard = new HeroCard()
      {
        Title = "Rivelino",
        Subtitle = "Roberto Rivelino",
        Images = new List<CardImage>
        {
          new CardImage("https://i.pinimg.com/originals/4f/75/5b/4f755b465ffa49ff63a79f2a436664d1.jpg", "Roberto Rivelino")
        },
        Buttons = new List<CardAction>
        {
          new CardAction(ActionTypes.OpenUrl, "Wikipedia", null, "https://pt.wikipedia.org/wiki/Roberto_Rivellino")
        }
      };

      return heroCard.ToAttachment();
    }

    //Apresentação de imagem pequeno
    private Attachment GetThumbnailCard()
    {
      var thumb = new ThumbnailCard()
      {
        Title = "Péle",
        Subtitle = "Rei Pele",
        Images = new List<CardImage>
        {
          new CardImage("http://www.defatoonline.com.br/noticias/img/noticias/foto_19072015093514.jpg", "Pelé")
        },
        Buttons = new List<CardAction>
        {
          new CardAction(ActionTypes.OpenUrl, "Wikipedia", null, "https://pt.wikipedia.org/wiki/Pel%C3%A9")
        }
      };

      return thumb.ToAttachment();
    }

    //Apresentação de imagem tipo Gif
    private Attachment GetAnimationCard()
    {
      var animation = new AnimationCard
      {
        Title = "Pelé",
        Subtitle = "Entortou o goleiro mas perdeu o gol.",
        Image = new ThumbnailUrl("http://www.defatoonline.com.br/noticias/img/noticias/foto_19072015093514.jpg"),
        Autostart = true,
        Autoloop = true,
        Media = new List<MediaUrl>
         {
           new MediaUrl("https://media.giphy.com/media/ZWaab6p2q95Yc/giphy.gif")
         }
      };

      return animation.ToAttachment();
    }

    //Apresentação de video tipo MP4
    private Attachment GetVideoCard()
    {
      var video = new VideoCard
      {
        Title = "Filme",
        Subtitle = "Video",
        Image = new ThumbnailUrl("http://www.defatoonline.com.br/noticias/img/noticias/foto_19072015093514.jpg"),
        Autostart = true,
        Autoloop = true,
        Media = new List<MediaUrl>
         {
           new MediaUrl("https://www.renderforest.com/pt/video-maker")
         },
        Buttons = new List<CardAction>
        {
          new CardAction
          {
            Title = "Full Screen",
            Value = "https://www.renderforest.com/pt/video-maker"

          }
        }
      };

      return video.ToAttachment();
    }

    //Apresentação de audio
    private Attachment GetAudioCard()
    {
      var audio = new AudioCard
      {
        Title = "Audio",
        Subtitle = "Utilizando Audio",
        Image = new ThumbnailUrl("http://www.pankadaosom.com.br/img/icones/som_automotivo/icon_alto_falante.png", "Falante"),
        Autostart = true,
        Autoloop = true,
        Media = new List<MediaUrl>
        {
          new MediaUrl("http://www.wavlist.com/movies/004/father.wav")
        }
      };
      return audio.ToAttachment();
    }
    //Lista de itens com imagens valores e total
    private Attachment GetReceiptCard()
    {
      var receipt = new ReceiptCard
      {
        Title = "Finalizar itens de compra",
        Facts = new List<Fact>
        {
          new Fact("Número da compra","1234"),
          new Fact("Endereço de Entrega","Rua das Alamedas"),
          new Fact("Forma de Pagamento","Cartão XPTOP final **** **** **** 3234"),
        },
        Items = new List<ReceiptItem>
        {
          new ReceiptItem("xbox one", "console", "Video Game",
                          new CardImage("http://support.xbox.com/Content/Images/XboxLogo.jpg"),
                          "R$: 1800.00",
                          "1 Console"),
           new ReceiptItem("Controle xbox one ",
                          image: new CardImage("https://compass-ssl.xbox.com/assets/d5/f0/d5f0f4c8-0dc4-47fa-8394-d426919017ab.jpg?n=Accessories_Panes-Triptic-Large-1400_Xbox-Wireless-Controller-Black_570x570.jpg"),
                          price: "R$: 200.00",
                          quantity: "1"),
           new ReceiptItem("Minecraft",
                          image: new CardImage("https://compass-ssl.xbox.com/assets/d6/f1/d6f12569-c03f-49d7-af97-8d0fa163538c.jpg?n=X1S-Minecraft-500GB_3-4-Col_Boxshot_740x417.jpg"),
                          price: "R$: 120.00",
                          quantity: "1"),
        },
        Tax = "R$ 800.00",
        Total = "R$ 2120.00",
        Buttons = new List<CardAction>
        {
          new CardAction(ActionTypes.OpenUrl, "Finalizar Compra")
        }
      };
      return receipt.ToAttachment();
    }

    //login
    private Attachment GetSigninCard()
    {
      var signin = new SigninCard
      {
        Text = "Login",
        Buttons = new List<CardAction>
        {
          new CardAction
          {
            Type = ActionTypes.Signin,
            Title = "Full Screen",
            Value = "https://login.microsoftonline.com/"
          }
        }
      };

      return signin.ToAttachment();
    }

  }
}