﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace CursoBot
{
  [BotAuthentication]
  public class MessagesController : ApiController
  {
    /// <summary>
    /// POST: api/Messages
    /// Receive a message from a user and reply to it
    /// </summary>
    /// 
    //Toda mensagem passa por aqui -> Web API Rest POST
    public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
    {
      // ActivityTypes.Message não e apenas menssagem pode ser um arquivo, anexo...
      if (activity.Type == ActivityTypes.Message)
      {
        //Verifica a menssagem que ele mandou e cria um dialogo com essa menssagem
        await Conversation.SendAsync(activity, () => new Dialogs.RootDialog());
      }
      else
      {
        //Verifica o tipo de mensagem e faz um tratamento espeficico
        HandleSystemMessage(activity);
      }
      var response = Request.CreateResponse(HttpStatusCode.OK);
      return response;
    }

    private Activity HandleSystemMessage(Activity message)
    {
      if (message.Type == ActivityTypes.DeleteUserData)
      {
        // Implement user deletion here
        // If we handle user deletion, return a real message
      }
      else if (message.Type == ActivityTypes.ConversationUpdate)
      {
        // Handle conversation state changes, like members being added and removed
        // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
        // Not available in all channels
      }
      else if (message.Type == ActivityTypes.ContactRelationUpdate)
      {
        // Handle add/remove from contact lists
        // Activity.From + Activity.Action represent what happened
      }
      else if (message.Type == ActivityTypes.Typing)
      {
        // Handle knowing tha the user is typing
      }
      else if (message.Type == ActivityTypes.Ping)
      {
      }

      return null;
    }
  }
}